﻿using HelloWorldApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HelloWorldApp.Controllers
{
    //test2
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var models = new List<UsersModel>();

            using (var db = new HelloWorldEntities())
            {
                var users = db.Users.ToList();
                foreach (var user in users)
                {
                    var model = new UsersModel
                    {
                        ID = user.Id,
                        UserName = user.UserName,
                        FirstName = user.FirstName,
                        LastName = user.LastName
                    };

                    models.Add(model);
                }
            }

            return View(models);
        }


        public ActionResult UserRecord()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserRecord(UsersModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new HelloWorldEntities())
                {
                    var newUser = new User
                    {
                        UserName = model.UserName,
                        FirstName = model.FirstName,
                        LastName = model.LastName
                    };

                    db.Users.Add(newUser);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
        }

        public ActionResult UserEditRecord(int ID)
        {
            using (var db = new HelloWorldEntities())
            {
                var user = db.Users.SingleOrDefault(x => x.Id == ID);
                if (user != null)
                {
                    var model = new UsersModel
                    {
                        ID = user.Id,
                        UserName = user.UserName,
                        FirstName = user.FirstName,
                        LastName = user.LastName
                    };
                    return View(model);
                }
            }
            return View("Error");
        }

        [HttpPost]
        public ActionResult UserEditRecord(UsersModel model)
        {
            if (ModelState.IsValid)
            {
                using (var db = new HelloWorldEntities())
                {
                    var user = db.Users.SingleOrDefault(x => x.Id == model.ID);
                    if (user != null)
                    {
                        user.UserName = model.UserName;
                        user.FirstName = model.FirstName;
                        user.LastName = model.LastName;
                        db.SaveChanges();
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return View();
        }


        public ActionResult UserDeleteRecord(int ID)
        {
            using (var db = new HelloWorldEntities())
            {
                var user = db.Users.SingleOrDefault(x => x.Id == ID);
                if (user != null)
                {
                    db.Users.Remove(user);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
            }
            return View("Error");
        }
    }
}