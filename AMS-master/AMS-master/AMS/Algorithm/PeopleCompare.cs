﻿using System;

namespace Algorithm
{
    public class PeopleCompare
    {
        public People People1 { get; set; }
        public People People2 { get; set; }
        public TimeSpan DateDiff { get; set; }
    }
}