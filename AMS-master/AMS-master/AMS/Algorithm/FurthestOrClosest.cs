﻿namespace Algorithm
{
    public enum FurthestOrClosest
    {
        Furthest,
        Closest
    }
}