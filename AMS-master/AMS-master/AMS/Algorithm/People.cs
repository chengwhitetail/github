﻿using System;

namespace Algorithm
{
    public class People
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}