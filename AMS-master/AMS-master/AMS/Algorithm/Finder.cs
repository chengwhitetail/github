﻿using System.Collections.Generic;

namespace Algorithm
{
    public class Finder
    {
        private readonly List<People> _peopleList;

        public Finder(List<People> peopleList)
        {
            _peopleList = peopleList;
        }

        public PeopleCompare Find(FurthestOrClosest ft)
        {
            var compareGroup = new List<PeopleCompare>();

            for(var i = 0; i < _peopleList.Count - 1; i++)
            {
                for(var j = i + 1; j < _peopleList.Count; j++)
                {
                    var peopleCompare = new PeopleCompare();
                    if (_peopleList[i].BirthDate < _peopleList[j].BirthDate)
                    {
                        peopleCompare.People1 = _peopleList[i];
                        peopleCompare.People2 = _peopleList[j];
                    }
                    else
                    {
                        peopleCompare.People1 = _peopleList[j];
                        peopleCompare.People2 = _peopleList[i];
                    }
                    peopleCompare.DateDiff = peopleCompare.People2.BirthDate - peopleCompare.People1.BirthDate;
                    compareGroup.Add(peopleCompare);
                }
            }

            if(compareGroup.Count < 1)
            {
                return new PeopleCompare();
            }

            PeopleCompare answer = compareGroup[0];
            foreach(var result in compareGroup)
            {
                switch(ft)
                {
                    case FurthestOrClosest.Closest:
                        if(result.DateDiff < answer.DateDiff)
                        {
                            answer = result;
                        }
                        break;

                    case FurthestOrClosest.Furthest:
                        if(result.DateDiff > answer.DateDiff)
                        {
                            answer = result;
                        }
                        break;
                }
            }

            return answer;
        }
    }
}