﻿using System;
using System.IO;
using System.Linq;

namespace ComplianceMapper
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter test file name:");

            string line = Console.ReadLine();

            generateOutputs(line);

            Console.Read();
        }

        public static int generateOutputs(string line)
        {
            var input = File.ReadAllLines(line);

            int count = 1;
            for (int i = 0; i < input.Count(); i += Convert.ToInt32(input[i].Split(' ')[0]) + 1)
            {
                if (i != input.Count() - 1)
                {
                    Console.WriteLine("Set #" + count);
                    buildMatrix(input.Skip(i + 1).Take(Convert.ToInt32(input[i].Split(' ')[0])).ToArray(), Convert.ToInt32(input[i].Split(' ')[0]), Convert.ToInt32(input[i].Split(' ')[1]));
                    Console.WriteLine();
                    count++;
                }
            }
            return count-1;
        }

        private static void buildMatrix(string[] input, int x, int y)
        {
            int i = 0, j = 0;
            var result = new string[x, y];
            foreach (var row in input)
            {
                j = 0;
                foreach (var col in row.ToCharArray())
                {
                    result[i, j] = col.ToString();
                    j++;
                }
                i++;
            }

            var outputs = new string[x, y];
            for (i = 0; i < x; i++)
            {
                for (j = 0; j < y; j++)
                {
                    if (result[i, j] == "*")
                    {
                        outputs[i, j] = "*";
                    }
                    else
                    {
                        outputs[i, j] = NeighbourCount(result, i, j).ToString();
                    }
                }
            }

            Print2DArray(outputs);
        }

        public static int NeighbourCount(string[,] array, int CellX, int CellY)
        {
            int count = 0;

            int minX = Math.Max(CellX - 1, array.GetLowerBound(0));
            int maxX = Math.Min(CellX + 1, array.GetUpperBound(0));
            int minY = Math.Max(CellY - 1, array.GetLowerBound(1));
            int maxY = Math.Min(CellY + 1, array.GetUpperBound(1));

            for (int x = minX; x <= maxX; x++)
            {
                for (int y = minY; y <= maxY; y++)
                {
                    if (array[x, y] == "*")
                        count++;
                }
            }
            return count;
        }

        public static void Print2DArray<T>(T[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}


