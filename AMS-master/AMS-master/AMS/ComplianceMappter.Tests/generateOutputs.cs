﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ComplianceMapper;

namespace ComplianceMappter.Tests
{
    [TestClass]
    public class generateOutputs
    {
        [TestMethod]
        public void Returns_test_input_txt()
        {
            int expected= 3;
            int outputs = Program.generateOutputs("test-input.txt");

            Assert.AreEqual(expected, outputs);
        }

        [TestMethod]
        public void Returns_large_test_txt()
        {
            int expected = 3;
            int outputs = Program.generateOutputs("large-test.txt");

            Assert.AreEqual(expected, outputs);
        }
    }
}
