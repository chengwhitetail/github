﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithm.Test
{
	[TestClass]
	public class FinderTests
	{
		[TestMethod]
		public void Returns_Empty_Results_When_Given_Empty_List()
		{
			var list = new List<People>();
			var finder = new Finder(list);

			var result = finder.Find(FurthestOrClosest.Closest);

			Assert.IsNull(result.People1);
			Assert.IsNull(result.People2);
		}

		[TestMethod]
		public void Returns_Empty_Results_When_Given_One_Person()
		{
			var list = new List<People>() { sue };
			var finder = new Finder(list);

			var result = finder.Find(FurthestOrClosest.Closest);

			Assert.IsNull(result.People1);
			Assert.IsNull(result.People2);
		}

		[TestMethod]
		public void Returns_Closest_Two_For_Two_People()
		{
			var list = new List<People>() { sue, greg };
			var finder = new Finder(list);

			var result = finder.Find(FurthestOrClosest.Closest);

			Assert.AreSame(sue, result.People1);
			Assert.AreSame(greg, result.People2);
		}

		[TestMethod]
		public void Returns_Furthest_Two_For_Two_People()
		{
			var list = new List<People>() { greg, mike };
			var finder = new Finder(list);

			var result = finder.Find(FurthestOrClosest.Furthest);

			Assert.AreSame(greg, result.People1);
			Assert.AreSame(mike, result.People2);
		}

		[TestMethod]
		public void Returns_Furthest_Two_For_Four_People()
		{
			var list = new List<People>() { greg, mike, sarah, sue };
			var finder = new Finder(list);

			var result = finder.Find(FurthestOrClosest.Furthest);

			Assert.AreSame(sue, result.People1);
			Assert.AreSame(sarah, result.People2);
		}

		[TestMethod]
		public void Returns_Closest_Two_For_Four_People()
		{
			var list = new List<People>() { greg, mike, sarah, sue };
			var finder = new Finder(list);

			var result = finder.Find(FurthestOrClosest.Closest);

			Assert.AreSame(sue, result.People1);
			Assert.AreSame(greg, result.People2);
		}

		People sue = new People() {Name = "Sue", BirthDate = new DateTime(1950, 1, 1)};
		People greg = new People() {Name = "Greg", BirthDate = new DateTime(1952, 6, 1)};
		People sarah = new People() { Name = "Sarah", BirthDate = new DateTime(1982, 1, 1) };
		People mike = new People() { Name = "Mike", BirthDate = new DateTime(1979, 1, 1) };
	}
}